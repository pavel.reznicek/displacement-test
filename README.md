# Displacement Test

This is a sample FreePascal project written to demonstrate the `encoding absolute displacement directly` warning/error raised while assembling an inline Motorola 68000 `asm ... end` block.

Supplied with:

- a [Lazarus project](./displacementtest.lpi) to make building comfortable

- an [alternative build script](./plainfpcbuild.sh) for plain FPC

- a [build log file](./build.log)

- a [generated assembler source](./displacementtest.s)
    
- the [compiled binary](./displacementtest.ttp)
