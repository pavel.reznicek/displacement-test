program displacementtest;

{$APPTYPE CONSOLE}

label Hello;
label Cont;
type
  TMyBuffer = array[0..1] of Char;
var
  TestText1, TestText2: TMyBuffer;

begin
  asm
    move.w #-2,d0
    jmp Cont
  Hello:
    dc.b 'H','i'
  Cont:
    move.w -2(pc,d0.w), TestText1
    move.w Hello, TestText2
  end;
  WriteLn('Test 1: ', TestText1);
  WriteLn('Test 2: ', TestText2);
  ReadLn;
end.

