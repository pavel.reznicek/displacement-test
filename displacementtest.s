	.file "displacementtest.lpr"
# Begin asmlist al_procedures

.section .text.n__main,"awx"
	.balignw 4,0x4e71
.globl	_main
_main:
.globl	PASCALMAIN
PASCALMAIN:
# [displacementtest.lpr]
# [12] begin
	link.w	%a6,#0
	jbsr	fpc_initializeunits
#  CPU 68000
# [14] move.w #-2,d0
	move.w	#-2,%d0
# [15] jmp Cont
	jra	.Lj3
.Lj4:
# [17] dc.b 'H','i'
	.ascii	"H"
	.ascii	"i"
.Lj3:
# [19] move.w -2(pc,d0.w), TestText1
	move.w	-2(%pc,%d0.w),U_$P$DISPLACEMENTTEST_$$_TESTTEXT1
# [20] move.w Hello, TestText2
	move.w	.Lj4,U_$P$DISPLACEMENTTEST_$$_TESTTEXT2
#  CPU 68000
# [22] WriteLn('Test 1: ', TestText1);
	jbsr	fpc_get_output
	move.l	%d0,%a2
	lea	_$DISPLACEMENTTEST$_Ld1,%a1
	lea	(%a2),%a0
	clr.l	%d0
	jbsr	fpc_write_text_shortstr
	jbsr	fpc_iocheck
	move.w	#1,-(%a7)
	lea	U_$P$DISPLACEMENTTEST_$$_TESTTEXT1,%a1
	lea	(%a2),%a0
	moveq.l	#1,%d1
	clr.l	%d0
	jbsr	fpc_write_text_pchar_as_array
	jbsr	fpc_iocheck
	lea	(%a2),%a0
	jbsr	fpc_writeln_end
	jbsr	fpc_iocheck
# [23] WriteLn('Test 2: ', TestText2);
	jbsr	fpc_get_output
	move.l	%d0,%a2
	lea	_$DISPLACEMENTTEST$_Ld2,%a1
	lea	(%a2),%a0
	clr.l	%d0
	jbsr	fpc_write_text_shortstr
	jbsr	fpc_iocheck
	move.w	#1,-(%a7)
	lea	U_$P$DISPLACEMENTTEST_$$_TESTTEXT2,%a1
	lea	(%a2),%a0
	moveq.l	#1,%d1
	clr.l	%d0
	jbsr	fpc_write_text_pchar_as_array
	jbsr	fpc_iocheck
	lea	(%a2),%a0
	jbsr	fpc_writeln_end
	jbsr	fpc_iocheck
# [24] ReadLn;
	jbsr	fpc_get_input
	move.l	%d0,%a2
	lea	(%a2),%a0
	jbsr	fpc_readln_end
	jbsr	fpc_iocheck
# [25] end.
	jbsr	fpc_do_exit
# End asmlist al_procedures
# Begin asmlist al_globals

.section .bss,"aw",%nobits
# [10] TestText1, TestText2: TMyBuffer;
U_$P$DISPLACEMENTTEST_$$_TESTTEXT1:
	.zero 2

.section .bss,"aw",%nobits
U_$P$DISPLACEMENTTEST_$$_TESTTEXT2:
	.zero 2

.section .data.n_INITFINAL,"aw"
	.balign 4
.globl	INITFINAL
INITFINAL:
	.long	1,0
	.long	INIT$_$SYSTEM
	.long	0

.section .data.n_FPC_THREADVARTABLES,"aw"
	.balign 4
.globl	FPC_THREADVARTABLES
FPC_THREADVARTABLES:
	.long	0

.section .data.n_FPC_RESOURCESTRINGTABLES,"aw"
	.balign 4
.globl	FPC_RESOURCESTRINGTABLES
FPC_RESOURCESTRINGTABLES:
	.long	0

.section .data.n_FPC_WIDEINITTABLES,"aw"
	.balign 4
.globl	FPC_WIDEINITTABLES
FPC_WIDEINITTABLES:
	.long	0

.section .data.n_FPC_RESSTRINITTABLES,"aw"
	.balign 4
.globl	FPC_RESSTRINITTABLES
FPC_RESSTRINITTABLES:
	.long	0

.section .fpc.n_version,"aw"
	.balign 4
__fpc_ident:
	.ascii	"FPC 3.3.1 [2022/10/01] for m68k - atari "

.section .data.n___stklen,"aw"
	.balign 4
.globl	__stklen
__stklen:
	.long	262144

.section .data.n___heapsize,"aw"
	.balign 4
.globl	__heapsize
__heapsize:
	.long	0

.section .data.n___fpc_valgrind,"aw"
	.balign 4
.globl	__fpc_valgrind
__fpc_valgrind:
	.byte	0
# End asmlist al_globals
# Begin asmlist al_typedconsts

.section .rodata.n__$DISPLACEMENTTEST$_Ld1,"aw"
	.balign 4
.globl	_$DISPLACEMENTTEST$_Ld1
_$DISPLACEMENTTEST$_Ld1:
# [22] WriteLn('Test 1: ', TestText1);
	.ascii	"\010Test 1: \000"

.section .rodata.n__$DISPLACEMENTTEST$_Ld2,"aw"
	.balign 4
.globl	_$DISPLACEMENTTEST$_Ld2
_$DISPLACEMENTTEST$_Ld2:
# [23] WriteLn('Test 2: ', TestText2);
	.ascii	"\010Test 2: \000"
# End asmlist al_typedconsts

